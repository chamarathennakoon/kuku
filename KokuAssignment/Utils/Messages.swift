//
//  Messages.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

class Messages{
    // MARK:- Titles
    static let success = "Success"
    static let error = "Error"
    
    // MARK:- Body
    static let turnOnNetwork = "Can not find a network. Please turn on the network"
    static let resetSuccess = "Password Reset is success"
    static let enterUsername = "Please enter username"
    static let enterPassword = "Please enter password"
    static let enterOldPassword = "Please enter your old password"
    static let enternewPasword = "Please enter your new password"
    static let enterConfirmPassword = "Confirm your password"
    static let confirmPasswordFail = "New password and confirm password are mismatched"
    static let changeSuccess = "Password Change is success"
    
    // MARK:- other
    static let pending = "Pending"
    static let competed = "Completed"
    static let pendingTransaction = "Pending Transactions"
    static let completedTransaction = "Completed Transactions"

    // MARK:- button
    static let ok = "Ok"
    
}
