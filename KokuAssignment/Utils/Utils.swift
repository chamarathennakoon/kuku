//
//  Utils.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/4/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import Reachability

class Utils{
    
    /**Check Network Availability*/
    static func isReachable() -> Bool{
        let reachability = Reachability()!
        
        switch reachability.connection {
        case .none:
            debugPrint("Network became unreachable")
            return false
            
        case .cellular, .wifi:
            debugPrint("Network reachable")
            return true
        }
    }
    
    // MARK:- get main storybord
    static func getStoryBoard() -> UIStoryboard{
        return UIStoryboard(name: "Main", bundle: Bundle.main)
    }
    
    // MARK:- read json file
    static func readJson(fileName: String) -> Data{
        if let path = Bundle.main.path(forResource: fileName, ofType: "json") {
            do {
                let data = try Data(contentsOf: URL(fileURLWithPath: path), options: .mappedIfSafe)
                    
                    return data
            } catch {
                // handle error
                return Data()
            }
        }
        
        return Data()
    }
}
