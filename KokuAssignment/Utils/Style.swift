//
//  CommonExtentions.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/3/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit

// MARK:- button extention
extension UIButton{
    
    func conerRoundButton(){
        self.layer.cornerRadius = 10
    }
}

// MARK:- view extention
extension UIView{
    
    func conerRoundView(){
        self.layer.cornerRadius = 10
    }
}
