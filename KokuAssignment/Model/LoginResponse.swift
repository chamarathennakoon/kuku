//
//  LoginResponse.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/4/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct LoginResponse: Decodable{
    var userId: String
    var token: String
    var responseStatus: ResponseStatus
    
}
