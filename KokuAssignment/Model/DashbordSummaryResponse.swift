//
//  DashbordSummaryResponse.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/6/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct DashbordSummaryResponse: Decodable{
    var pendingTransaction: String
    var competedTransaction: String
    var profit: String
    var responseStatus: ResponseStatus
    
}
