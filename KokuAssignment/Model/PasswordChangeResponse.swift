//
//  PasswordChangeResponse.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/4/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct PasswordChangeResponse: Decodable{
    var responseStatus: ResponseStatus
    
}
