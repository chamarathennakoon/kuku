//
//  LoginRequset.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/4/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct LoginRequest: Encodable{
    var userName: String
    var password: String
    
    init(userName: String, password: String) {
        self.userName = userName
        self.password = password
    }
}
