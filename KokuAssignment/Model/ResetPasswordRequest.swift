//
//  ResetPasswordRequest.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/5/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct ResetPasswordRequest: Encodable{
    var password: String
    
    init(password: String) {
        self.password = password
    }
}
