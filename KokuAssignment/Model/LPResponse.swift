//
//  LPResponse.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct LPResponse: Decodable{
    var lpName: String
    var currencyType: String
    var buy: String
    var sell: String
    
}
