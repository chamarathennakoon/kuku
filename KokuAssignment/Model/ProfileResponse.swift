//
//  ProfileResponse.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/4/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct ProfileResponse: Decodable{
    var name: String
    var email: String
    var mobileNumber: String
    var systemRole: String
    var responseStatus: ResponseStatus
    
}
