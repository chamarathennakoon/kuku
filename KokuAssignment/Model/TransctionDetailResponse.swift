//
//  TransctionDetailResponse.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/6/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct TranscationDetailResponse: Decodable{
    var transactionList: [TransactionDetail]
    var responseStatus: ResponseStatus
    
}

struct TransactionDetail: Decodable{
    var customerName: String
    var customerNumber: String
    var fromAmount: String
    var fromCurrency: String
    var toAmount: String
    var toCurrency: String
    var fee: String
    var paybleAmount: String
    var transactionStatus: String
    
}
