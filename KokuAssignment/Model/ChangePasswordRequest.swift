//
//  ChangePasswordRequest.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/5/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct ChangePasswordRequest: Encodable{
    var oldPassword: String
    var newPassword: String
    var confirmPassword: String
    
    init(oldPassword: String, newPassword: String, confirmPassword: String) {
        self.oldPassword = oldPassword
        self.newPassword = newPassword
        self.confirmPassword = confirmPassword
    }
}
