//
//  ResponseStatus.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/6/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct ResponseStatus: Decodable{
    var status: String
    var errorCode: String
    var error: String
    
}
