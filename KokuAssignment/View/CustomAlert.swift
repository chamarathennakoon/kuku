//
//  CustomAlert.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/5/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit

class CustomAlert{
    
    // MARK:- single button alert
    static func noActionAlert(viewController: UIViewController, title:String, message: String, buttonText: String){
        let alert = UIAlertController(title: title, message: message, preferredStyle: UIAlertController.Style.alert)
        alert.addAction(UIAlertAction(title: buttonText, style: UIAlertAction.Style.default, handler: nil))
        viewController.present(alert, animated: true, completion: nil)
    }
}
