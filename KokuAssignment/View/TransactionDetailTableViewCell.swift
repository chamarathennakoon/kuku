//
//  TransactionDetailTableViewCell.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit

class TransactionDetailTableViewCell: UITableViewCell{
    // MARK:- IBOutlets
    @IBOutlet weak var customerName: UILabel!
    @IBOutlet weak var customerNumber: UILabel!
    @IBOutlet weak var currencyType: UILabel!
    @IBOutlet weak var amount: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
