//
//  LPTableViewCell.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit

class LPTableViewCell: UITableViewCell {
    // MARK:- IBOutlets
    @IBOutlet weak var lpNameLable: UILabel!
    @IBOutlet weak var lpCurrencyLable: UILabel!
    @IBOutlet weak var buyLable: UILabel!
    @IBOutlet weak var sellLable: UILabel!

    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
