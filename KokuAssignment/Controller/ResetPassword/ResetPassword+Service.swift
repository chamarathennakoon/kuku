//
//  ResetPassword+Service.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/5/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit
import KRProgressHUD

extension ResetPasswordViewController{
    
    //Mark:- create reset password requset
    func createRequest() -> ResetPasswordRequest?{
        guard let password = passwordTF.text, !password.isEmpty else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.enterPassword, buttonText: Messages.ok)
            
            return nil
        }
        
        let resetPasswordRequset = ResetPasswordRequest(password: password)
        
        return resetPasswordRequset
    }
    
    //Mark:- send password reset request
    func sendRequest(){
        do{
            
            guard  let body = createRequest() else {
                return
            }
            
            let encoded = try JSONEncoder().encode(body)
            
            guard let requestBody =  String(data: encoded, encoding: .utf8) else{
                print("Request cannot convert to string")
                return
            }
            
            KRProgressHUD.show()
            
            ApiManager.shared.sendData(body: requestBody, url: ServiceConfig.passwordChangeURL) { (result) in
                KRProgressHUD.dismiss()
                
                self.HandleResponseSuccess(result: result.data!)
            }
            
        }catch{
            print("Error: \(error)")
        }
    }
    
    //Mark:- populate paasword reset success
    func HandleResponseSuccess(result: Data){
        do{
            let response = try JSONDecoder().decode(PasswordChangeResponse.self, from: result)
            
            if(response.responseStatus.status == "success"){
           CustomAlert.noActionAlert(viewController: self, title: Messages.success, message: Messages.resetSuccess, buttonText: Messages.ok)
                
                passwordTF.text = ""
            }
        }catch{
            print(error)
        }
    }
}
