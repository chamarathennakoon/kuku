//
//  ResetPasswordViewController.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/3/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ResetPasswordViewController: BaseViewController{
    // MARK:- IBOutlets
    @IBOutlet weak var passwordTF: SkyFloatingLabelTextField!
    @IBOutlet weak var resetBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        resetBtn.conerRoundButton()
    }
    
    //Mark:- Action
    @IBAction func resetButtonClicked(_ sender: UIButton) {
        if(Utils.isReachable()){
            sendRequest()
        }
        else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.turnOnNetwork, buttonText: Messages.ok)
        }
    }
}
