//
//  Login+Service.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/4/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit
import KRProgressHUD

extension LoginViewController{
    
    //Mark:- create login request
    func createRequest() -> LoginRequest?{

        guard let userName = userNameTF.text, !userName.isEmpty else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.enterUsername, buttonText: Messages.ok)
            
            return nil
        }
        
        guard let password = passwordTF.text, !password.isEmpty else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.enterPassword, buttonText: Messages.ok)
            
            return nil
        }
        
        let loginRequest = LoginRequest(userName: userName, password: password)
        
        return loginRequest
    }
    
    //Mark:- send login request
    func sendRequest(){
        do{
            guard  let body = createRequest() else {
                return
            }
            
            let encoded = try JSONEncoder().encode(body)
            guard let requestBody =  String(data: encoded, encoding: .utf8) else{
                print("Request cannot convert to string")
                return
            }
            
            KRProgressHUD.show()
            
            ApiManager.shared.sendData(body: requestBody, url: ServiceConfig.loginURL) { (result) in
                KRProgressHUD.dismiss()
                
                self.HandleResponseSuccess(result: result.data!)
            }
            
        }catch{
            print("Error: \(error)")
        }
    }
    
    //Mark:- get action for response data
    func HandleResponseSuccess(result: Data){
        do{
            let response = try JSONDecoder().decode(LoginResponse.self, from: result)
            
            if(response.responseStatus.status == "success"){
                userNameTF.text = ""
                passwordTF.text = ""
                
            let dashboardVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "DashboardViewController") as! DashboardViewController
            self.navigationController?.pushViewController(dashboardVC, animated: true)
            }
        }catch{
            print(error)
        }
    }
}
