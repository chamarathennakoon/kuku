//
//  Login+extention.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/3/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit

extension LoginViewController: UITextFieldDelegate{

    //Mark:- create maximum length for TextField
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let userNameMaxinumLength: Int = 15
        let passwordMaximumLength: Int = 20

        guard let text = textField.text else { return true }
        let textNewLength = text.count + string.count - range.length

        let characterSet = CharacterSet.alphanumerics
        if string.rangeOfCharacter(from: characterSet.inverted) != nil {
            return false
        }else{
            switch(textField){
            case userNameTF:
                return textNewLength < userNameMaxinumLength
            case passwordTF:
                return textNewLength < passwordMaximumLength
            default:
                return true
            }
        }
    }
}
