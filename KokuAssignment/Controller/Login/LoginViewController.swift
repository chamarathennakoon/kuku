//
//  LoginViewController.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 1/31/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class LoginViewController: UIViewController{
    // MARK:- IBOutlets
    @IBOutlet weak var userNameTF: SkyFloatingLabelTextField!
    @IBOutlet weak var passwordTF: SkyFloatingLabelTextField!
    @IBOutlet weak var loginBtn: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        loginBtn.conerRoundButton()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }

    //Mark:- Action
    @IBAction func loginClicked(_ sender: UIButton) {
        if(Utils.isReachable()){
            sendRequest()
        }
        else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.turnOnNetwork, buttonText: Messages.ok)
        }
    }
}

