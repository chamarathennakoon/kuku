//
//  ChangePassword+Service.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/5/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit
import KRProgressHUD

extension ChangePassowordViewController{
    
    //Mark:- create password change request
    func createRequest() -> ChangePasswordRequest?{
        guard let oldPassword = oldPassword.text, !oldPassword.isEmpty else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.enterOldPassword, buttonText: Messages.ok)
            
            return nil
        }
        
        guard let newPassword = newPassword.text, !newPassword.isEmpty else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.enternewPasword, buttonText: Messages.ok)
            
            return nil
        }
        
        guard let confirmPassword = confirmNewPassword.text, !confirmPassword.isEmpty else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.enterConfirmPassword, buttonText: Messages.ok)
            
            return nil
        }
        
        if(newPassword != confirmPassword){
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.confirmPasswordFail, buttonText: Messages.ok)
            
            return nil
        }
        
        let changePasswordRequest = ChangePasswordRequest(oldPassword: oldPassword, newPassword: newPassword, confirmPassword: confirmPassword)
        
        return changePasswordRequest
    }
    
    //Mark:- send password change requset
    func sendRequest(){
        do{
            
            guard  let body = createRequest() else {
                return
            }
            
            let encoded = try JSONEncoder().encode(body)
            guard let requestBody =  String(data: encoded, encoding: .utf8) else{
                print("Request cannot convert to string")
                return
            }
            
            KRProgressHUD.show()
            
            ApiManager.shared.sendData(body: requestBody, url: ServiceConfig.passwordChangeURL) { (result) in
                KRProgressHUD.dismiss()
                
                self.HandleResponseSuccess(result: result.data!)
            }
            
        }catch{
            print("Error: \(error)")
        }
    }
    
    //Mark:- populate password change success
    func HandleResponseSuccess(result: Data){
        do{
            let response = try JSONDecoder().decode(PasswordChangeResponse.self, from: result)
            
            if(response.responseStatus.status == "success"){
                CustomAlert.noActionAlert(viewController: self, title: Messages.success, message: Messages.changeSuccess, buttonText: Messages.ok)
                
                oldPassword.text = ""
                newPassword.text = ""
                confirmNewPassword.text = ""
            }
        }catch{
            print(error)
        }
       
    }
}
