//
//  ChangePassword+TextField.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/3/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit

extension ChangePassowordViewController: UITextFieldDelegate{
    
    //Mark:- create maximum length for TextField
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        let maximumLength: Int = 20
        
        guard let text = textField.text else { return true }
        let textNewLength = text.count + string.count - range.length
        
        let characterSet = CharacterSet.alphanumerics
        
        if string.rangeOfCharacter(from: characterSet.inverted) != nil {
            return false
        }else{
            switch(textField){
            case oldPassword:
                return textNewLength < maximumLength
            case newPassword:
                return textNewLength < maximumLength
            case confirmNewPassword:
                return textNewLength < maximumLength
            default:
                return true
            }
        }
    }
}
