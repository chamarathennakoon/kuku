//
//  ChangePassowordViewController.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/3/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit
import SkyFloatingLabelTextField

class ChangePassowordViewController: BaseViewController{
    // MARK:- IBOutlets
    @IBOutlet weak var oldPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var newPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var confirmNewPassword: SkyFloatingLabelTextField!
    @IBOutlet weak var changebtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        changebtn.conerRoundButton()
    }
    
    //Mark:- Action
    @IBAction func changeButtonClicked(_ sender: UIButton) {
        if(Utils.isReachable()){
            sendRequest()
        }
        else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.turnOnNetwork, buttonText: Messages.ok)
        }
    }
}
