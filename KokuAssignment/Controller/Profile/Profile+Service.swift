//
//  Profile+Service.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/5/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit
import KRProgressHUD

extension ProfileViewController{

    //Mark:- get profile data
    func sendRequset(){
        KRProgressHUD.show()
        
        ApiManager.shared.sendData(body: "", url: ServiceConfig.profileURL) { (result) in
            KRProgressHUD.dismiss()
            
            self.HandleResponseSuccess(result: result.data!)
        }
    }
    
    //Mark:- populate response profile data
    func HandleResponseSuccess(result: Data){
        do{
           let response = try JSONDecoder().decode(ProfileResponse.self, from: result)
            
            if(response.responseStatus.status == "success"){
                nameLable.text = response.name
                emailLable.text = response.email
                mobileNumberLable.text = response.mobileNumber
                systemRoleLable.text = response.systemRole
            }
        }catch{
            print(error)
        }
    }
}
