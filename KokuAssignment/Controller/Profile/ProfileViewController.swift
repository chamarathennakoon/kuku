//
//  ProfileViewController.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/3/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit

class ProfileViewController: BaseViewController{
    // MARK:- IBOutlets
    @IBOutlet weak var nameLable: UILabel!
    @IBOutlet weak var emailLable: UILabel!
    @IBOutlet weak var systemRoleLable: UILabel!
    @IBOutlet weak var mobileNumberLable: UILabel!
    @IBOutlet weak var changePassword: UIButton!
    @IBOutlet weak var resetPassword: UIButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
        sendRequset()
    }
    
    // MARK:- Create view
    func setUpView(){
        changePassword.conerRoundButton()
        resetPassword.conerRoundButton()
    }
}
