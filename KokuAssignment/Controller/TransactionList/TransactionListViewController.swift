//
//  TransactionListViewController.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit

class TransactionListViewController: BaseViewController{
    // MARK:- IBOutlets
    @IBOutlet weak var transactionTableView: UITableView!
    
    // MARK:- Globle veriables
    var transactionType: Int = 1
    var transactionList: [TransactionDetail] = []

    override func viewDidLoad() {
        super.viewDidLoad()

        sendRequest()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        
        self.navigationItem.title = getPageTitle(trasactionType: transactionType)
    }
    
    // MARK:- get page Title
    func getPageTitle(trasactionType: Int) -> String{
        if(trasactionType == 1){
            return Messages.pendingTransaction //Pending transaction title
        }
        else{
            return Messages.completedTransaction //Completed transaction title
        }
    }
}
