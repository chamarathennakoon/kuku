//
//  TransactionList+TableView.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit

extension TransactionListViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return transactionList.count
    }

    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
   // MARK:- fill table row data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let item = transactionList[indexPath.row]
        
        let cell: TransactionDetailTableViewCell = tableView.dequeueReusableCell(withIdentifier: "TransactionDetailTableViewCell") as! TransactionDetailTableViewCell
        
        cell.customerName.text = item.customerName
        cell.customerNumber.text = item.customerNumber
        cell.amount.text = item.paybleAmount
        cell.currencyType.text = item.fromCurrency
            
        return cell
    }
    
    // MARK:- table row select action
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        let storyBoard = UIStoryboard(name: "Main", bundle: Bundle.main)
        let transactionsummaryVC = storyBoard.instantiateViewController(withIdentifier: "TransactionSummaryViewController") as! TransactionSummaryViewController
        transactionsummaryVC.transaction = transactionList[indexPath.row]
        transactionsummaryVC.transactionType = transactionType
        self.navigationController?.pushViewController(transactionsummaryVC, animated: true)
    }  
}
