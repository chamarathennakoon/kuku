//
//  TransactionList+Service.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import KRProgressHUD

extension TransactionListViewController{
    
    // MARK:- send requset to get transaction list
    func sendRequest(){
        KRProgressHUD.show()
        
        ApiManager.shared.sendData(body: "", url: getRequestURL(transactionType: self.transactionType)) { (result) in
            KRProgressHUD.dismiss()
            
            self.HandleResponseSuccess(result: result.data!)
        }
    }
    
    // MARK:- populate transaction list data
    func HandleResponseSuccess(result: Data){
        do{
            print(NSString(string: String(data: result, encoding: .utf8) ?? ""))
            let response = try JSONDecoder().decode(TranscationDetailResponse.self, from: result)
            
            if(response.responseStatus.status == "success"){
                transactionList = response.transactionList
                
                transactionTableView.reloadData()
            }
        }catch{
            print(error)
        }
    }
    
    func getRequestURL(transactionType: Int) -> String{
        if(transactionType == 1){
            return ServiceConfig.transactionPendingURL //pending transactions request url
        }
        else{
            return ServiceConfig.transactionCompletedURL //completed transaction request url
        }
    }
}
