//
//  DashboardViewController.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/2/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit
import SideMenu

class DashboardViewController: UIViewController {
    // MARK:- Globle veriables
    private var menuLeftNavigationController:UISideMenuNavigationController?
    
    // MARK:- IBOutlets
    @IBOutlet weak var pendingLable: UILabel!
    @IBOutlet weak var competedLable: UILabel!
    @IBOutlet weak var profitLable: UILabel!
    @IBOutlet weak var lpTableView: UITableView!
    @IBOutlet weak var pendingView: UIView!
    @IBOutlet weak var CompletedView: UIView!
    @IBOutlet weak var profitView: UIView!
    @IBOutlet weak var pendingMoreBtn: UIButton!
    @IBOutlet weak var competedMoreBtn: UIButton!

    override func viewDidLoad() {
        super.viewDidLoad()

        configureNavigationBar()
        setUPView()
        sendRequest()
    }
    
    // MARK:- Create view
    func setUPView(){
        pendingView.conerRoundView()
        CompletedView.conerRoundView()
        profitView.conerRoundView()
        pendingMoreBtn.conerRoundButton()
        competedMoreBtn.conerRoundButton()
    }
    
    //Mark:- Action
    @objc func profileButtonPressed(){
        if(Utils.isReachable()){
            let profileVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "ProfileViewController") as! ProfileViewController
            self.navigationController?.pushViewController(profileVC, animated: true)
        }
        else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.turnOnNetwork, buttonText: Messages.ok)
        }
    }
    
    @objc func logoutButtonPressed(){
        self.navigationController?.popViewController(animated: true)
    }
    
    @IBAction func pendingMoreClicked(_ sender: UIButton) {
        if(Utils.isReachable()){
            let transactionListVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "TransactionListViewController") as! TransactionListViewController
            transactionListVC.transactionType = 1
            self.navigationController?.pushViewController(transactionListVC, animated: true)
        }
        else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.turnOnNetwork, buttonText: Messages.ok)
        }
    }
    
    
    @IBAction func completedMoreClicked(_ sender: UIButton) {
        if(Utils.isReachable()){
            let transactionListVC = Utils.getStoryBoard().instantiateViewController(withIdentifier: "TransactionListViewController") as! TransactionListViewController
            transactionListVC.transactionType = 2
            self.navigationController?.pushViewController(transactionListVC, animated: true)
        }
        else{
            CustomAlert.noActionAlert(viewController: self, title: Messages.error, message: Messages.turnOnNetwork, buttonText: Messages.ok)
        }
    }

    // MARK:- configure navigation bar
    func configureNavigationBar(){
        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.isNavigationBarHidden = false
        
        let profileButton: UIButton = UIButton (type: UIButton.ButtonType.custom)
        profileButton.setImage(UIImage(named: "user"), for: .normal)
        profileButton.addTarget(self, action: #selector(profileButtonPressed), for: UIControl.Event.touchUpInside)
        profileButton.frame = CGRect(x:0, y:0, width:30, height:30)
        let liftBarButton = UIBarButtonItem(customView: profileButton)
        
        self.navigationItem.leftBarButtonItem = liftBarButton
        
        let logoutButton: UIButton = UIButton (type: UIButton.ButtonType.custom)
        logoutButton.setImage(UIImage(named: "logout"), for: .normal)
        logoutButton.addTarget(self, action: #selector(logoutButtonPressed), for: UIControl.Event.touchUpInside)
        logoutButton.frame = CGRect(x:0, y:0, width:30, height:30)
        let rightBarButton = UIBarButtonItem(customView: logoutButton)
        
        self.navigationItem.rightBarButtonItem = rightBarButton
    }
}
