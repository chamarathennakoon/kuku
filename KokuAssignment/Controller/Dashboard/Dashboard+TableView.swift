//
//  Dashboard+TableView.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import UIKit

extension DashboardViewController: UITableViewDelegate, UITableViewDataSource{
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return 5
    }
    
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 125
    }
    
    // MARK:- fill table row data
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        let cell: LPTableViewCell = tableView.dequeueReusableCell(withIdentifier: "LPTableViewCell") as! LPTableViewCell
        
        var isSelect: Bool = true
        
        switch indexPath.row {
        case 0:
            // MARK:- Add real time polling behaviour to LP Partner 1
            Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
                if(isSelect){
                    self.updateLPLater(cell: cell, fileName: "lp6")
                }
                else{
                    self.updateLPLater(cell: cell, fileName: "lp1")
                }
                                    isSelect = !isSelect
            }
            
            return updateLP(cell: cell, fileName: "lp1")
            
        case 1:
            // MARK:- Add real time polling behaviour to LP Partner 2
            Timer.scheduledTimer(withTimeInterval: 8, repeats: true) { (timer) in
                if(isSelect){
                    self.updateLPLater(cell: cell, fileName: "lp7")
                }
                else{
                    self.updateLPLater(cell: cell, fileName: "lp2")
                }
                isSelect = !isSelect
            }
            
            return updateLP(cell: cell, fileName: "lp2")
        case 2:
            // MARK:- Add real time polling behaviour to LP Partner 3
            Timer.scheduledTimer(withTimeInterval: 6, repeats: true) { (timer) in
                if(isSelect){
                    self.updateLPLater(cell: cell, fileName: "lp8")
                }
                else{
                    self.updateLPLater(cell: cell, fileName: "lp3")
                }
                isSelect = !isSelect
            }
            
            return updateLP(cell: cell, fileName: "lp3")
        case 3:
            // MARK:- Add real time polling behaviour to LP Partner 4
            Timer.scheduledTimer(withTimeInterval: 5, repeats: true) { (timer) in
                if(isSelect){
                    self.updateLPLater(cell: cell, fileName: "lp9")
                }
                else{
                    self.updateLPLater(cell: cell, fileName: "lp4")
                }
                isSelect = !isSelect
            }
            
            return updateLP(cell: cell, fileName: "lp9")
        case 4:
            // MARK:- Add real time polling behaviour to LP Partner 5
            Timer.scheduledTimer(withTimeInterval: 8, repeats: true) { (timer) in
                if(isSelect){
                    self.updateLPLater(cell: cell, fileName: "lp10")
                }
                else{
                    self.updateLPLater(cell: cell, fileName: "lp5")
                }
                isSelect = !isSelect
            }
            
            return updateLP(cell: cell, fileName: "lp5")
        default:
            break
        }
        
        return cell
    }
    
    // MARK:- update lps initial values
    func updateLP(cell: LPTableViewCell, fileName: String) -> UITableViewCell{
        let lp = Utils.readJson(fileName: fileName)
        
        do{
            let response = try JSONDecoder().decode(LPResponse.self, from: lp)
            
            cell.lpNameLable.text = response.lpName
            cell.lpCurrencyLable.text = response.currencyType
            cell.buyLable.text = response.buy
            cell.sellLable.text = response.sell
            
            return cell
        }catch{
            print(error)
            
            return UITableViewCell()
        }
    }
    
    // MARK:- real time update lps values
    func updateLPLater(cell: LPTableViewCell, fileName: String){
        let lp = Utils.readJson(fileName: fileName)
        
        do{
            let response = try JSONDecoder().decode(LPResponse.self, from: lp)
            
            cell.lpNameLable.text = response.lpName
            cell.lpCurrencyLable.text = response.currencyType
            cell.buyLable.text = response.buy
            cell.sellLable.text = response.sell
        }catch{
            print(error)
        }
    }
    
}
