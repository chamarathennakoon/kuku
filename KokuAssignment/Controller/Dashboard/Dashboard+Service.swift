//
//  Dashboard+Service.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/7/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import KRProgressHUD

extension DashboardViewController{
    
    // MARK:- get dashboard data
    func sendRequest(){
            KRProgressHUD.show()
            
            ApiManager.shared.sendData(body: "", url: ServiceConfig.dashboardURL) { (result) in
                KRProgressHUD.dismiss()
                
                self.HandleResponseSuccess(result: result.data!)
            }
    }
    
    // MARK:- Populate dashboard data
    func HandleResponseSuccess(result: Data){
        do{
            let response = try JSONDecoder().decode(DashbordSummaryResponse.self, from: result)
            
            if(response.responseStatus.status == "success"){
                pendingLable.text = response.pendingTransaction
                competedLable.text = response.competedTransaction
                profitLable.text = "USD\n" + response.profit
            }
        }catch{
            print(error)
        }
    }
}
