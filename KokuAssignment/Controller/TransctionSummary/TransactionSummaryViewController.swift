//
//  TransactionSummaryViewController.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/6/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import UIKit

class TransactionSummaryViewController: BaseViewController{
    // MARK:- IBOutlets
    @IBOutlet weak var customerNameLable: UILabel!
    @IBOutlet weak var customerNumberLable: UILabel!
    @IBOutlet weak var fromAmountLable: UILabel!
    @IBOutlet weak var fromCurrencyLable: UILabel!
    @IBOutlet weak var toAmountLable: UILabel!
    @IBOutlet weak var toCurrencyLable: UILabel!
    @IBOutlet weak var feeLable: UILabel!
    @IBOutlet weak var paybleAmountLable: UILabel!
    @IBOutlet weak var transactionStatusLable: UILabel!
    @IBOutlet weak var dashboardBtn: UIButton!
    
    // MARK:- Globle veriables
    var transaction: TransactionDetail?
    var transactionType: Int = 1

    override func viewDidLoad() {
        super.viewDidLoad()

        setUpView()
    }
    
    // MARK:- Create view
    func setUpView(){
        customerNameLable.text = transaction?.customerName
        customerNumberLable.text = transaction?.customerNumber
        fromAmountLable.text = transaction?.fromAmount
        fromCurrencyLable.text = transaction?.fromCurrency
        toAmountLable.text = transaction?.toAmount
        toCurrencyLable.text = transaction?.toCurrency
        feeLable.text = transaction?.fee
        paybleAmountLable.text = transaction?.paybleAmount
        
        if(transactionType == 1){
            transactionStatusLable.text = Messages.pending
        }
        else{
            transactionStatusLable.text = Messages.competed
        }
        
        dashboardBtn.conerRoundButton()
    }

    //Mark:- Action
    @IBAction func DashbordButtonClicked(_ sender: UIButton) {
        for controller in self.navigationController!.viewControllers as Array {
            if controller.isKind(of: DashboardViewController.self) {
                self.navigationController!.popToViewController(controller, animated: true)
                break
            }
        }
    }
}
