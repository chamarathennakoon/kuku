//
//  ServiceConfig.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/4/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation

struct ServiceConfig{
    public static let loginURL = "https://api.myjson.com/bins/ij9kg"
    public static let passwordChangeURL = "https://api.myjson.com/bins/8ewa8"
    public static let profileURL = "https://api.myjson.com/bins/9lrhs"
    public static let transactionPendingURL = "https://api.myjson.com/bins/14molk"
    public static let transactionCompletedURL = "https://api.myjson.com/bins/10isiw"
    public static let dashboardURL = "https://api.myjson.com/bins/118xvs"
    
    static let SERVER_TIMEOUT: Double = 90
    
}
