//
//  ApiManager.swift
//  KokuAssignment
//
//  Created by Chamara Thennakoon on 2/4/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import Foundation
import Alamofire

class ApiManager{
    
    static var shared = ApiManager()
    var manager: Alamofire.SessionManager = {
        
        // Create custom manager
        let configuration = URLSessionConfiguration.default
        configuration.httpAdditionalHeaders = Alamofire.SessionManager.defaultHTTPHeaders
        configuration.timeoutIntervalForRequest = ServiceConfig.SERVER_TIMEOUT
        let manager = Alamofire.SessionManager(
            configuration: configuration
        )
        return manager
    }()
    
    // MARK:- send alemofire request
    func sendData(body: String, enableActivityIndicator: Bool = true, url: String, completion:@escaping (DataResponse<Any>) -> ()) {
        
        let headers: HTTPHeaders = ["Content-Type":"application/json"]
        
        let request = manager.request(url, method: .get, parameters: nil, encoding: JSONEncoding.default, headers: headers)

        request.responseJSON(completionHandler: { (jsonResponse) in
            completion(jsonResponse)
        })
    }
}

// MARK:- Errors
enum APIError: Error {
    case internalError(code: Int)
    case alamofireError(code: Int)
    case timeOut()
    case httpError(statusCode: Int)
    case NoNetwork()
    case apiResponseError(responseCode: String)
    
}
