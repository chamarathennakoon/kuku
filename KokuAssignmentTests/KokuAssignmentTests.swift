//
//  KokuAssignmentTests.swift
//  KokuAssignmentTests
//
//  Created by Chamara Thennakoon on 1/31/19.
//  Copyright © 2019 Chamara. All rights reserved.
//

import XCTest
@testable import KokuAssignment

class KokuAssignmentTests: XCTestCase {

    override func setUp() {
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }

    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
    }

    // MARK:- test title is correct
    func testGetPageTitle(){
        let transactionListVC = TransactionListViewController()

        var title: String = Messages.pendingTransaction
        var transactionType: Int = 1

        XCTAssertEqual(transactionListVC.getPageTitle(trasactionType: transactionType), title)

        title = Messages.completedTransaction
        transactionType = 2

        XCTAssertEqual(transactionListVC.getPageTitle(trasactionType: transactionType), title)
    }
    
    // MARK:- get transaction detail request url
    func testGetRequsetURL(){
        let transactionListVC = TransactionListViewController()
        
        var url: String = ServiceConfig.transactionPendingURL
        var transactionType: Int = 1
        
        XCTAssertEqual(transactionListVC.getRequestURL(transactionType: transactionType), url)
        
        url = ServiceConfig.transactionCompletedURL
        transactionType = 2
        
        XCTAssertEqual(transactionListVC.getRequestURL(transactionType: transactionType), url)
    }
}
